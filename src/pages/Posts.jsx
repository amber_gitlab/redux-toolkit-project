import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import {
  VisibilityOutlined,
  CreateOutlined,
  DeleteOutlineOutlined,
} from "@mui/icons-material";
import LoadingButton from "@mui/lab/LoadingButton";
import {
  Typography,
  IconButton,
  CircularProgress,
  Box,
  Snackbar,
} from "@mui/material";
import MuiAlert from "@mui/material/Alert";
import { fetchPosts } from "../redux/reducers/postsSlice";
import { deletePost } from "../redux/reducers/deletePostSlice";

import "./index.css";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const Posts = () => {
  const [loader, setLoader] = useState(false);
  const [postId, setPostId] = useState("");
  const [state, setState] = useState({
    open: false,
    vertical: "top",
    horizontal: "center",
  });
  const [allPosts, setAllPosts] = useState([]);
  const posts = useSelector((state) => state.posts);
  const dispatch = useDispatch();

  const { vertical, horizontal, open } = state;

  console.log("all posts", posts);

  useEffect(() => {
    dispatch(fetchPosts());
  }, []);

  useEffect(() => {
    if (!posts.loading && !posts.error && posts.posts.length) {
      setAllPosts(posts.posts);
    }
  }, [posts, posts.posts.length]);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setState({ ...state, open: false });
  };

  const deleteItem = (id, newState) => {
    setLoader(true);
    setPostId(id);
    dispatch(deletePost(id)).then((res) => {
      console.log("del res", res);
      if (res.payload.status === 200) {
        setState({ open: true, ...newState });
        setLoader(false);
      }
    });
  };

  if (posts.error !== "") {
    return (
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <Typography variant="h5">{posts.error}</Typography>
      </Box>
    );
  }
  if (posts.loading) {
    return (
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <CircularProgress />
      </Box>
    );
  }
  return (
    <>
      <div className="container">
        <div className="posts__container">
          {allPosts.map((items) => (
            <div className="posts" key={items.id}>
              <div className="title__section">
                <Typography variant="subtitle1" className="posts__title">
                  {items.title.charAt(0).toUpperCase() +
                    items.title.slice(1, 12)}
                </Typography>

                <div className="post__title-icons">
                  <Link to={`/posts/${items.id}`}>
                    <IconButton
                      aria-label="view"
                      className="icons__btn"
                      size="small"
                      color="success"
                    >
                      <VisibilityOutlined />
                    </IconButton>
                  </Link>
                  <Link to={`/edit-posts/${items.id}`}>
                    <IconButton
                      aria-label="edit"
                      className="icons__btn"
                      size="small"
                      color="success"
                    >
                      <CreateOutlined />
                    </IconButton>
                  </Link>

                  <IconButton
                    aria-label="delete"
                    className="icons__btn"
                    size="small"
                    color="error"
                    onClick={() =>
                      deleteItem(items.id, {
                        vertical: "bottom",
                        horizontal: "right",
                      })
                    }
                  >
                    {loader && items.id === postId ? (
                      <CircularProgress size={20} />
                    ) : (
                      <DeleteOutlineOutlined />
                    )}
                  </IconButton>
                </div>
              </div>

              <div className="posts__body">
                <Typography variant="body1">
                  {`${
                    items.body.charAt(0).toUpperCase() +
                    items.body.slice(1, 100)
                  }...`}
                </Typography>
              </div>
            </div>
          ))}
        </div>
      </div>
      <div>
        <Snackbar
          anchorOrigin={{ vertical, horizontal }}
          open={open}
          autoHideDuration={4000}
          onClose={handleClose}
          key={vertical + horizontal}
        >
          <Alert
            onClose={handleClose}
            severity="success"
            sx={{ width: "100%" }}
          >
            Post Deleted Successfully!
          </Alert>
        </Snackbar>
      </div>
    </>
  );
};

export default Posts;
