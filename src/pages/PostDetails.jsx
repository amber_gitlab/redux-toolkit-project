import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { Typography, CircularProgress, Box } from "@mui/material";
import { fetchPost } from "../redux/reducers/postSlice";

import "./index.css";

const PostDetails = () => {
  const [singlePost, setSinglePost] = useState({});
  const { id } = useParams();
  const dispatch = useDispatch();
  const post = useSelector((state) => state.post);

  useEffect(() => {
    dispatch(fetchPost(id));
  }, []);

  useEffect(() => {
    setSinglePost(post);
  }, [post]);

  console.log("post", post);
  if (post.error !== "") {
    return (
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <Typography variant="h5">{post.error}</Typography>
      </Box>
    );
  }
  if (post.loading) {
    return (
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <CircularProgress />
      </Box>
    );
  }
  return (
    <div>
      {!post.loading && post.error === "" && post.post && (
        <div className="posts">
          <div className="title__section">
            <Typography variant="subtitle1" className="posts__title">
              {post.post.title.charAt(0).toUpperCase() +
                post.post.title.slice(1)}
            </Typography>
          </div>

          <div className="posts__body">
            <Typography variant="body1">
              {post.post.body.charAt(0).toUpperCase() + post.post.body.slice(1)}
            </Typography>
          </div>
        </div>
      )}
    </div>
  );
};

export default PostDetails;
