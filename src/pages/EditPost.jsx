import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";
import {
  Typography,
  CircularProgress,
  Box,
  TextField,
  Snackbar,
  Button,
} from "@mui/material";
import MuiAlert from "@mui/material/Alert";
import LoadingButton from "@mui/lab/LoadingButton";
import { CreateOutlined } from "@mui/icons-material";
import { fetchPost } from "../redux/reducers/postSlice";
import { editPost } from "../redux/reducers/editPostSlice";

import "./index.css";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const EditPost = () => {
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [loader, setLoader] = useState(false);
  const [state, setState] = React.useState({
    open: false,
    vertical: "top",
    horizontal: "center",
  });
  const [singlePost, setSinglePost] = useState({});
  const { id } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const post = useSelector((state) => state.post);

  const { vertical, horizontal, open } = state;

  const editBody = {
    userId: id,
    id,
    title,
    body,
  };

  useEffect(() => {
    dispatch(fetchPost(id));
  }, []);

  useEffect(() => {
    if (!post.loading && post.error === "" && post.post) {
      setSinglePost(post.post);
    }
  }, [post.post]);

  useEffect(() => {
    if (!post.loading && post.error === "" && post.post) {
      setTitle(singlePost.title);
      setBody(singlePost.body);
    }
  }, [post.post]);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setState({ ...state, open: false });
  };

  const handleTitleChange = (event) => {
    setTitle(event.target.value);
  };
  const handleBodyChange = (event) => {
    setBody(event.target.value);
  };

  const updatePost = (newState) => {
    console.log("edit body", editBody);
    setLoader(true);
    dispatch(editPost(id, editBody)).then((res) => {
      console.log("resp", res);
      if (res.payload.status === 200) {
        setLoader(false);
        setState({ open: true, ...newState });
        setTimeout(() => {
          navigate("/");
        }, 4000);
      }
    });
  };

  console.log("post", post);
  return (
    <div>
      {!post.loading && post.error === "" && post.post && (
        <div className="edit__container">
          <div className="edit__posts">
            <div className="edit__title-section">
              <TextField
                id="outlined-multiline-static"
                label="Title"
                multiline
                rows={2}
                fullWidth
                onChange={handleTitleChange}
                defaultValue={
                  post.post.title.charAt(0).toUpperCase() +
                  post.post.title.slice(1)
                }
              />
            </div>

            <div className="edit__posts-body">
              <TextField
                id="outlined-multiline-static"
                label="Body"
                multiline
                rows={4}
                fullWidth
                onChange={handleBodyChange}
                defaultValue={
                  post.post.body.charAt(0).toUpperCase() +
                  post.post.body.slice(1)
                }
              />
            </div>
            <div className="update-btn">
              <LoadingButton
                variant="outlined"
                startIcon={<CreateOutlined />}
                loadingPosition="start"
                loading={loader}
                onClick={() =>
                  updatePost({
                    vertical: "bottom",
                    horizontal: "right",
                  })
                }
              >
                Update
              </LoadingButton>
              <Snackbar
                anchorOrigin={{ vertical, horizontal }}
                open={open}
                autoHideDuration={6000}
                onClose={handleClose}
              >
                <Alert
                  onClose={handleClose}
                  severity="success"
                  sx={{ width: "100%" }}
                >
                  post Updated Successfully!
                </Alert>
              </Snackbar>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default EditPost;
