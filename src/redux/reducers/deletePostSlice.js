import axios from "axios";
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
  loading: true,
  deletePostResult: [],
  error: "",
};

// Generates pending, fulfilled and rejected action types
export const deletePost = createAsyncThunk("post/deletePost", (id) => {
  return axios
    .delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
    .then((response) => {
      console.log(response);
      return response;
    });
});

const deletePostSlice = createSlice({
  name: "delete post",
  initialState,
  extraReducers: (builder) => {
    builder.addCase(deletePost.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(deletePost.fulfilled, (state, action) => {
      state.loading = false;
      state.deletePostResult = action.payload.data;
      state.error = "";
    });
    builder.addCase(deletePost.rejected, (state, action) => {
      state.loading = false;
      state.deletePostResult = [];
      state.error = action.error.message;
    });
  },
});

export default deletePostSlice.reducer;
