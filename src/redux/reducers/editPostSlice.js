import axios from "axios";
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
  loading: true,
  updatedPost: [],
  error: "",
};

// Generates pending, fulfilled and rejected action types
export const editPost = createAsyncThunk("post/editPost", (id, editBody) => {
  return axios
    .put(`https://jsonplaceholder.typicode.com/posts/${id}`, editBody)
    .then((response) => {
      console.log(response);
      return response;
    });
});

const editPostSlice = createSlice({
  name: "edit post",
  initialState,
  extraReducers: (builder) => {
    builder.addCase(editPost.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(editPost.fulfilled, (state, action) => {
      state.loading = false;
      state.updatedPost = action.payload.data;
      state.error = "";
    });
    builder.addCase(editPost.rejected, (state, action) => {
      state.loading = false;
      state.updatedPost = [];
      state.error = action.error.message;
    });
  },
});

export default editPostSlice.reducer;
