import { configureStore } from "@reduxjs/toolkit";
import postsReducer from "./reducers/postsSlice";
import postReducer from "./reducers/postSlice";
import editPostReducer from "./reducers/editPostSlice";
import deletePostReducer from "./reducers/deletePostSlice";

const store = configureStore({
  reducer: {
    posts: postsReducer,
    post: postReducer,
    updatedPostData: editPostReducer,
    deletePost: deletePostReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export default store;
