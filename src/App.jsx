import React from "react";
import { Route, Routes } from "react-router-dom";
import Posts from "./pages/Posts";
import PostDetails from "./pages/PostDetails";
import EditPost from "./pages/EditPost";

const App = () => {
  return (
    <Routes>
      <Route path="/" element={<Posts />} />
      <Route path="/posts/:id" element={<PostDetails />} />
      <Route path="/edit-posts/:id" element={<EditPost />} />
    </Routes>
  );
};

export default App;
